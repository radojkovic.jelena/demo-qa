**Demo QA**

This project contains several Test Scenarios and Test Cases taken on pages of website https://demoqa.com/.
Framework Cypress is used for writing these automated tests, and all the code is written in JavaScript.
For more information about Cypress, click the link https://www.cypress.io/.

**Instalation**
1. npm install

**Test** 
After successful installation of modules, to run Test Cases use the following command:
1. npm run test

NOTE: for local run you can also use the command: npx cypress open
