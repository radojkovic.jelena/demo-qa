export class RadioButton_Page{
    // locators
    yesRadio_locator = "#yesRadio";
    impressiveRadio_locator = "#impressiveRadio";
    noRadio_locator = "#noRadio";
    outputText_locator = ".mt-3";
    // methods
    navigate(url){
        cy.visit(url);
    }

    clickYes(){
        cy.get(this.yesRadio_locator)
            .check({force: true})
            .should("be.checked");
        this.validateLabel("Yes");
    }

    clickImpressive(){
        cy.get(this.impressiveRadio_locator)
            .check({force: true})
            .should("be.checked");
        this.validateLabel("Impressive");
    }

    hoverNo(){
        cy.get(this.noRadio_locator)
            .should("be.disabled");
    }

    validateLabel(labelName){
        var outputLabel = "You have selected " + labelName;
        cy.get(this.outputText_locator)
            .should("have.text", outputLabel);
    }
}