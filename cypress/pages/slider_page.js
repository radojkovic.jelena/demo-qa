export class Slider_Page{
    // locators
    slider_locator = "input[type='range']";
    label_locator = ".range-slider__tooltip__label";
    sliderVal_locator = "#sliderValue";
    // methods
    navigate(url){
        cy.visit(url);
    }
    
    setNewValue(number){
        const nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value').set;
        cy.get(this.slider_locator).then(($range) => {
            const range = $range[0];
            nativeInputValueSetter.call(range, number);
            range.dispatchEvent(new Event('change', { value: number, bubbles: true }));
          }
        );
    }
    
    newValueVisible(number){
        if(number < 0){
            number = 0;
        }else if(number > 100){
            number = 100;
        }
        cy.get(this.label_locator)
            .should('have.text', number)
        cy.get(this.sliderVal_locator)
            .should('have.value', number)
    }
}