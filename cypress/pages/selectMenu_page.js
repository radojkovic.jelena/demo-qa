export class SelectMenu_Page{
    // locators
    arrowDown_locator;
    input_locator;
    singleValue_locator = ".css-1uccc91-singleValue";
    expandedBody_locator = ".css-11unzgr";
    oldStyleSelect_locator = "#oldSelectMenu";
    multiSelect_locator = "#cars";
    removeAll_locator = ":nth-child(1) > .css-19bqh2r";
    indicatorContainer = " > .css-yk16xz-control > .css-1wy0on6 > .css-tlfecz-indicatorContainer";
    cars = ["volvo", "saab", "opel", "audi"];
    colors = ["Green", "Blue", "Black", "Red"];
    titles = ["Dr.", "Mr.", "Mrs.", "Ms.", "Prof.", "Other"];
    // methods
    navigate(url){
        cy.visit(url);
    }

    expandDropDown(){
        cy.get(this.arrowDown_locator)
            .click();
    }   

    expandedBodyExists(){
        cy.get(this.expandedBody_locator)
            .should("exist");
    }

    selectOption(group, num, option){
        let locator = this.getOption(group, num, option);
        cy.get(locator)
            .click();
    }

    optionSelected(group, num, option = null){
        let text;
        if(num > 2){
            if(num == 3){
                text = this.getOneSelectText(group);
            }else if(num == 4){
                text = this.getMultiDropDownText(group);
            }
        }else{
            text = this.getText(group, option);
        }
        cy.get(this.singleValue_locator)
            .should("contain", text);
    }

    selectedOptiNotVisible(group, num, option){
        let locator = this.getOption(group, num, option);
        cy.get(locator)
            .should("not.exist");
    }

    oldStyleSelectOption(option){
        option = option.toString();
        cy.get(this.oldStyleSelect_locator)
            .select(option);
    }
    
    oldStyleOptionSelected(option){
        cy.get(this.oldStyleSelect_locator)
            .should("have.value", option);
    }

    selectTwoOptions(index1, index2){
        let option1 = this.cars[index1];
        let option2 = this.cars[index2];
        cy.get(this.multiSelect_locator)
            .select([option1, option2])
    }

    twoOptionsSelected(index1, index2){
        cy.get(this.multiSelect_locator)
        .invoke("val")
        .should("contain", this.cars[index1])
        .and("contain", this.cars[index2]);
    }

    selectAllOptions(){
        cy.get(this.multiSelect_locator)
            .select([this.cars[0], this.cars[1], this.cars[2], this.cars[3]])
    }

    allOptionsSelected(){
        cy.get(this.multiSelect_locator)
            .invoke("val")
            .should("deep.equal", [this.cars[0], this.cars[1], this.cars[2], this.cars[3]]);
    }

    typeOption(group, num, option = null){
        let text;
        if(num > 2){
            if(num == 3){
                text = this.getOneSelectText(group);
            }else if(num == 4){
                text = this.getMultiDropDownText(group);
            }
        }else{
            text = this.getText(group, option);
        }
        cy.get(this.input_locator)
            .type(text + "{downarrow}{enter}", {force: true})
    }

    removeAll(){
        cy.get(this.removeAll_locator)
            .click();
    }

    allOptionsVisible(){
        cy.get(this.getOption(0, 4))
                .should("exist");
        cy.get(this.getOption(1, 4))
                .should("exist");
        cy.get(this.getOption(2, 4))
                .should("exist");
        cy.get(this.getOption(3, 4))
                .should("exist");
    }

    getOneSelectText(option){
        return this.titles[option];;
    }

    getMultiDropDownText(option){
        return this.colors[option];
    }

    getText(group, option){
        let text;
        if(group == 2){
            text = "A root option";
        }else if(group == 3){
            text = "Another root option";
        }else{
            text = "Group " + (group + 1) + ", option " + (option + 1);
        }
        return text;
    }

    getOption(group, num, option = null){
        let locator;
        if(num > 2)
        {
            if(num == 3){
                locator = "#react-select-" + num + "-option-0-" + group;
            }else if(num == 4){
                locator = "#react-select-" + num + "-option-" + group;
            }
        }
        else
        {
            locator = "#react-select-" + num + "-option-" + group;
            if(group < 2)
            {
                locator =  locator + "-" + option;
            }    
        }
        return locator;
    }

    setLocators(type){
        switch (type) {
            case "selectValue":
                this.arrowDown_locator = "#withOptGroup" + this.indicatorContainer;                
                this.input_locator = "#react-select-2-input";
                break;
            case "selectOne":
                this.arrowDown_locator = "#selectOne" + this.indicatorContainer;
                this.input_locator = "#react-select-3-input";
                break;
            case "multiSelectDropDown":
                this.arrowDown_locator = ":nth-child(7) > .col-md-6 > .css-2b097c-container" + this.indicatorContainer;
                this.singleValue_locator = ".css-1pahdxg-control > .css-1hwfws3";
                this.input_locator = "#react-select-4-input";
                break;
            default:
                break;
        }
    }
}