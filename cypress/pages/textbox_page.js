export class TextBox_Page{
    // locators
    fullName_locator = "#userName";
    email_locator = "#userEmail";
    currentAddress_locator = "#currentAddress";
    permanentAddress_locator = "#permanentAddress";
    submit_locator = "#submit";
    output_locator = ".border";
    outputName_locator = "#name";
    outputEmail_locator = "#email";
    outputCurrentAddress_locator = ".border > #currentAddress";
    outputPermanentAddress_locator = ".border > #permanentAddress";
    // globals
    email_global = "";
    name_global = "";
    currentAddress_global = "";
    permanentAddress_global = "";
    // methods
    navigate(url){
        cy.visit(url);
    }

    enterFullName(fullName){
        this.name_global = fullName;
        cy.get(this.fullName_locator)
            .type(fullName);
    }

    enterEmail(email){
        this.email_global = email;
        cy.get(this.email_locator)
            .type(email);
    }

    emailShouldMatchRegEx(){
        expect(this.email_global).to.match(/([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})/);
    }

    emailShouldNotMatchRegEx(){
        expect(this.email_global).not.to.match(/([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})/);
    }

    emailBorderNeutral(){
        cy.get(this.email_locator).should("have.class", "mr-sm-2 form-control");
    }

    emailBorderRed(){
        cy.get(this.email_locator).should("have.class", "mr-sm-2 field-error form-control");
    }

    enterCurrentAddress(currentAddress) {
        this.currentAddress_global = currentAddress;
        cy.get(this.currentAddress_locator)
            .type(currentAddress);
    }

    enterPermanentAddress(permanentAddress){
        this.permanentAddress_global = permanentAddress;
        cy.get(this.permanentAddress_locator)
            .type(permanentAddress);
    }
    
    clickSubmit(){
        cy.get(this.submit_locator)
            .click();
    }

    outputVisible(){
        cy.get(this.output_locator).should("exist");
    }

    outputInvisible(){
        cy.get(this.output_locator).should("not.exist");
    }

    validateOutputName(){
        cy.get(this.outputName_locator)
            .should('have.text', "Name:" +  this.name_global);
    }

    validateOutputEmail(){
        cy.get(this.outputEmail_locator)
            .should('have.text', "Email:" + this.email_global);
    }
    
    validateOutputCurrentAddress(){
        cy.get(this.outputCurrentAddress_locator)
            .should('have.text', "Current Address :" + this.currentAddress_global + " ");
    }

    validateOutputPermanentAddress(){
        cy.get(this.outputPermanentAddress_locator)
            .should('have.text', "Permananet Address :" + this.permanentAddress_global);
    }
}