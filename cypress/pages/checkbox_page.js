export class CheckBox_Page{
    // locators
    result_locator = "#result";
    // locators/dropdown
    expandAll_locator = ".rct-option-expand-all";
    collapseAll_locator = ".rct-option-collapse-all > .rct-icon";
    allExpanedOl_locator = "#tree-node > :nth-child(2) > :nth-child(1) > :nth-child(2)";
    homeExpand_locator = ".rct-collapse";
    desktopExpand_locator = ".rct-node-expanded > ol > :nth-child(1) > .rct-text > .rct-collapse";
    documentsExpand_locator = ":nth-child(2) > .rct-text > .rct-collapse";
    downloadsExpand_locator = ":nth-child(3) > .rct-text > .rct-collapse";
    homeCollapse_locator = "#tree-node > :nth-child(2) > :nth-child(1) > :nth-child(1) > .rct-collapse";
    desktopCollapse_locator = "#tree-node > :nth-child(2) > :nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(1) > .rct-collapse";
    documentsCollapse_locator = "#tree-node > :nth-child(2) > :nth-child(1) > :nth-child(2) > :nth-child(2) > :nth-child(1) > .rct-collapse";
    downloadsCollapse_locator = ":nth-child(3) > :nth-child(1) > .rct-collapse";
    // locators/checkboxes
    notesCheckbox_locator = "#tree-node-notes";
    commandsCheckbox_locator = "#tree-node-commands";
    desktopCheckbox_locator = "#tree-node-desktop";
    // methods
    navigate(url){
        cy.visit(url);
    }

    expandAll(){
        cy.get(this.expandAll_locator)
            .click();
        cy.get(this.allExpanedOl_locator)
            .should("exist");
    }

    collapseAll(){
        cy.get(this.collapseAll_locator)
            .click();
        cy.get(this.allExpanedOl_locator)
            .should("not.exist");
    }
    // methods/checks
    checkNotes(){
        cy.get(this.notesCheckbox_locator)
            .check({force: true})
            .should("be.checked");

        cy.get(this.result_locator)
            .contains("notes");
    }

    uncheckNotes(){
        cy.get(this.notesCheckbox_locator)
            .uncheck({force: true})
            .should("not.be.checked");
        cy.get(this.desktopCheckbox_locator)
            .should("not.be.checked");
    }

    checkCommands(){
        cy.get(this.commandsCheckbox_locator)
            .check({force: true})
            .should("be.checked");
        cy.get(this.result_locator)
            .contains("commands");
    }

    uncheckCommands(){
        cy.get(this.commandsCheckbox_locator)
            .uncheck({force: true})
            .should("not.be.checked");
        cy.get(this.desktopCheckbox_locator)
            .should("not.be.checked");
    }

    checkNotesAndCommands(){
        this.checkNotes();
        this.checkCommands();
        cy.get(this.desktopCheckbox_locator)
            .should("be.checked");
        cy.get(this.result_locator)
            .contains("desktop");
    }

    checkDesktop(){
        cy.get(this.desktopCheckbox_locator)
            .check({force: true});
        cy.get(this.notesCheckbox_locator)
            .should("be.checked");
        cy.get(this.commandsCheckbox_locator)
            .should("be.checked");
    }
    // methods/expands
    homeListExpand(){
        cy.get(this.homeExpand_locator)
            .click();
    }

    desktopListExpand(){
        cy.get(this.desktopExpand_locator)
            .click();
    }

    documentsListExpand(){
        cy.get(this.documentsExpand_locator)
            .click({multiple: true});
    }
    
    downloadsListExpand(){
        cy.get(this.downloadsExpand_locator)
            .click();
    }
    // methods/collapses
    homeListCollapse(){
        cy.get(this.homeCollapse_locator)
            .click();
    }

    desktopListCollapse(){
        cy.get(this.desktopCollapse_locator)
            .click();
    }

    documentsListCollapse(){
        cy.get(this.documentsCollapse_locator)
            .click();
    }

    downloadsListCollapse(){
        cy.get(this.downloadsCollapse_locator)
            .click();
    }
}