export class Date_Page{
    // locators
    datePicker_locator;
    calendar_locator;
    selectMonth_locator;
    selectYear_locator;
    monthYearLabel_locator;
    dateInput_locator;
    yearList_locator;
    spanSelected_locator;
    selectedYear_locator;
    monthDropDown_locator;
    monthList_locator;
    monthOption_locator;
    selectedMonth_locator;
    timeOption_locator;
    day_locator = ".react-datepicker__day--0";
    buttonNext_locator = ".react-datepicker__navigation--next";
    buttonPrev_locator = ".react-datepicker__navigation--previous";
    // globals
    day_global = "";
    month_global = "";
    year_global = "";
    hour_global = "";
    minute_global = "";
    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    // methods
    navigate(url){
        cy.visit(url);
    }

    clickDatePicker(){
        cy.get(this.datePicker_locator)
            .click();
    }

    calendarShouldExist(){
        cy.get(this.calendar_locator)
            .should("exist");
    }

    calendarShouldNotExist(){
        cy.get(this.calendar_locator)
            .should("not.exist");
    }

    selectYear(year){
        this.year_global = year;
        year = year.toString();
        cy.get(this.selectYear_locator)
            .select(year)
            .should("have.value", year);
    }

    expandYearDropDown(){
        cy.get(this.yearDropDown_locator)
            .click();
    }

    yearDropDownExpanded(){
        cy.get(this.yearList_locator)
            .should("be.visible");
    }

    selectYearDropDown(child){
        cy.get(this.yearOption_locator + "(" + child + ")")
            .click();
    }

    yearSelected(child){
        let year = 2026 - child + 2;
        this.year_global = year;
        year = year.toString();
        cy.get(this.selectedYear_locator)
            .should("have.text", year);
        this.expandYearDropDown();
        cy.get(this.spanSelected_locator)
            .should("be.visible");
    }

    expandMonthDropDown(){
        cy.get(this.monthDropDown_locator)
            .click({force: true});
    }

    monthDropDownExpanded(){
        cy.get(this.monthList_locator)
            .should("be.visible");
    }

    selectMonthDropDown(child){
        this.month_global = child-1;
        cy.get(this.monthOption_locator + "(" + child + ")")
            .click();
    }

    monthSelected(child){
        let monthName = this.getMonthNameFromIndex(child - 1);
        cy.get(this.selectedMonth_locator)
            .should("have.text", monthName);
    }

    validateYearLabel(){
        cy.get(this.monthYearLabel_locator)
            .contains(this.year_global);
    }
    
    selectMonth(monthOption){
        let monthIndex = monthOption - 1;
        let monthName = this.getMonthNameFromIndex(monthIndex);
        this.month_global = monthOption;
        monthIndex = monthIndex.toString();
        cy.get(this.selectMonth_locator)
            .select(monthName)
            .should("have.value",  monthIndex);
    }

    validateMonthLabel(index){
        let monthName = this.getMonthNameFromIndex(index - 1);
        cy.get(this.monthYearLabel_locator)
            .contains(monthName);
    }

    chooseDay(day){
        this.day_global = day;
        if(day < 10){ 
            day = "0" +  day;
        }
        cy.get(this.day_locator + day)
            .first()
            .click();
    }

    chooseTime(hour, minute){
        this.hour_global = hour;
        this.minute_global = minute;
        let time = hour + ":" + minute
        cy.get(this.timeOption_locator)
            .contains(time)
                .click();
    }

    validateDisplayedDateAndTime(){
        let monthName = this.getMonthNameFromIndex(this.month_global);
        let time = this.formatTimeOutput(this.hour_global, this.minute_global);
        let date = monthName + " " + this.day_global + ", " + this.year_global;
        cy.get(this.datePicker_locator)
            .should("contain.value", date + " " + time);
    }

    formatTimeOutput(hour, minute){
        let text, short;
        hour < 12 ? short = "AM" : short = "PM";
        if(hour == 0){
            hour = 12;
        }
        else if(hour > 12){
            hour -= 12;
        }
        
        if(minute == 0){
            minute += '0';
        }

        text = hour + ":" + minute + " " + short;
        return text;
    }

    validateDisplayedDate(){
        if(this.month_global < 10){
            this.month_global = "0" + this.month_global;
        }
        if(this.day_global < 10){
            this.day_global = "0" + this.day_global;
        }
        let text = this.month_global + "/" + this.day_global + "/" + this.year_global;
        cy.get(this.datePicker_locator)
            .should("have.value", text)
    }

    clickArrowNext(){
        cy.get(this.buttonNext_locator)
            .click();
    }

    clickArrowPrevious(){
        cy.get(this.buttonPrev_locator)
            .click();
    }

    getNextMonthIndex(currentOption){
        let currentIndex = currentOption - 1;
        let nextIndex = 0;
        if(currentIndex == 11)
        {
            nextIndex = 0;
        }
        else
        {
            nextIndex = currentIndex + 1;
        }
        return nextIndex;
    }

    getPreviousMonthIndex(currentOption){
        let currentIndex = currentOption - 1;
        let prevIndex = 0;
        if(currentIndex == 0)
        {
            prevIndex = 11;
        }
        else
        {
            prevIndex = currentIndex - 1;
        }
        return prevIndex;
    }

    validateSelectedMonthIndex(index, time = null){
        if(time == null){
            cy.get(this.selectMonth_locator)
                .should("have.value", index);
        }else{
            let monthName = this.getMonthNameFromIndex(index);
            cy.get(this.selectedMonth_locator)
                .invoke("text")
                .should("contain", monthName);
        }
    }
    
    getMonthIndexFromName(monthName){
        for(let i = 0; i < 12; i++){
            if(this.months[i] == monthName){
                return i;
            }
        }
        return -1;
    }

    getMonthNameFromIndex(index){
        return this.months[index];
    }

    setLocators(key){
        switch (key) {
            case "date":
                this.datePicker_locator = "#datePickerMonthYearInput";
                this.calendar_locator = ".react-datepicker__month-container";
                this.selectMonth_locator = ".react-datepicker__month-select";
                this.selectYear_locator = ".react-datepicker__year-select";
                this.monthYearLabel_locator = ".react-datepicker__current-month";
                this.day_locator = ".react-datepicker__day--0";
                this.buttonNext_locator = ".react-datepicker__navigation--next";
                this.buttonPrev_locator = ".react-datepicker__navigation--previous";
                break;
            case "time":
                this.datePicker_locator = "#dateAndTimePickerInput";
                this.calendar_locator = ".react-datepicker";
                this.selectMonth_locator = ".react-datepicker__month-read-view";
                this.monthYearLabel_locator = ".react-datepicker__current-month";
                this.yearOption_locator = ".react-datepicker__year-dropdown > :nth-child";
                this.yearDropDown_locator = ".react-datepicker__year-read-view--down-arrow";
                this.yearList_locator = ".react-datepicker__year-dropdown";
                this.spanSelected_locator = ".react-datepicker__year-option--selected";
                this.selectedYear_locator = ".react-datepicker__year-read-view--selected-year";
                this.monthDropDown_locator = ".react-datepicker__month-read-view--down-arrow";
                this.monthList_locator = ".react-datepicker__month-dropdown";
                this.monthOption_locator = ".react-datepicker__month-dropdown > :nth-child";
                this.selectedMonth_locator = ".react-datepicker__month-read-view--selected-month";
                this.timeOption_locator = ".react-datepicker__time-list > li";
            default:
                break;
        }
    }
}