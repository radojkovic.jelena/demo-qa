export class Links_Page{
    // locators
    homeLink_locator = "#simpleLink";
    dynamicLink_locator = "#dynamicLink";
    createdLink_locator = "#created";
    noContent_locator = "#no-content";
    moved_locator = "#moved";
    badRequest_locator = "#bad-request";
    unauthorized_locator = "#unauthorized";
    forbidden_locator = "#forbidden";
    notFound_locator = "#invalid-url";
    response_locator = "#linkResponse";
    // methods
    navigate(url){
        cy.visit(url);
    }
    // new tab
    clickHome(){
        cy.get(this.homeLink_locator)
            .click();
        // cypress ne podrzava vise tabova?
    }

    clickHomerAF6n(){
        cy.get(this.dynamicLink_locator)
            .click();
    }
    // API call
    APIcall(link, status){
        cy.request('https://demoqa.com/' + link)
            .then((response) => {
                expect(response.status).to.equal(status);
                cy.get(this.response_locator).should("contain", response.status);
            });
    }

    clickCreated(){
        cy.get(this.createdLink_locator)
            .click();
        this.APIcall("created", 201);
        cy.get(this.response_locator)
            .should("contain", "Created");
    }

    clickNoContent(){
        cy.get(this.noContent_locator)
            .click();
        this.APIcall("no-content", 204);
        cy.get(this.response_locator)
            .should("contain", "No Content");
    }

    clickMoved(){
        cy.get(this.moved_locator)
            .click();
        this.APIcall("moved", 301);
        cy.get(this.response_locator)
            .should("contain", "Moved Permanently");
    }

    clickBadRequest(){
        cy.get(this.badRequest_locator)
            .click();
        cy.get(this.response_locator)
            .should("contain", "Bad Request");
        //this.APIcall("bad-request", 400);
    }

    clickUnauthorized(){
        cy.get(this.unauthorized_locator)
            .click();
        this.APIcall("unauthorized", 401);
        cy.get(this.response_locator)
            .should("contain", "Unauthorized");
    }

    clickForbidden(){
        cy.get(this.forbidden_locator)
            .click();
        this.APIcall("forbidden", 403);
        cy.get(this.response_locator)
            .should("contain", "Forbidden");
    }

    clickNotFound(){
        cy.get(this.notFound_locator)
            .click();
        this.APIcall("invalid-url", 403);
        cy.get(this.response_locator)
            .should("contain", "Not Found");
    }
}