export class WebTables_Page{
    // locators
    row_locator = ".rt-tr-group";
    edit_locator = "#edit-record-";
    delete_locator = "#delete-record-";
    close_locator = ".close";
    firstName_locator = "#firstName";
    lastName_locator = "#lastName";
    email_locator = "#userEmail";
    age_locator = "#age";
    salary_locator = "#salary";
    department_locator = "#department";
    submitButton_locator = "#submit";
    addButton_locator = "#addNewRecordButton";
    searchbox_locator = "#searchBox";
    searchButton_locator = "#basic-addon2";
    dialogBox_locator = ".modal-content";    
    // methods
    navigate(url){
        cy.visit(url);
    }

    getRowPath(i){
        return ".rt-tbody > :nth-child(" + i + ")";
    }

    getTdPath(i, j){
        return this.getRowPath(i) + " > .rt-tr > :nth-child(" + j + ")";
    }

    clickTd(i, j){
        cy.get(this.getTdPath(i, j))
            .click();
    }

    deleteRow(row){
        cy.get(this.delete_locator + row)
            .click();
        cy.get(this.delete_locator + row)
            .should("not.exist");
    }

    openEditWindow(row){
        cy.get(this.edit_locator + row)
            .click();
        cy.get(this.dialogBox_locator)
            .should("exist");
    }

    closeEditWindow(){
        cy.get(this.close_locator)
            .click();
        cy.get(this.dialogBox_locator)
            .should("not.exist");
    }

    typeFirstName(firstName){
        cy.get(this.firstName_locator)
            .clear();
        cy.get(this.firstName_locator)
            .type(firstName);
    }

    typeLastName(lastName){
        cy.get(this.lastName_locator)
            .clear();
        cy.get(this.lastName_locator)
            .type(lastName);
    }

    editFirstAndLastName(row, firstName, lastName){
        this.openEditWindow(row);
        this.typeFirstName(firstName);
        this.typeLastName(lastName);
        cy.get(this.submitButton_locator)
            .click();
        cy.get(this.getTdPath(row, 1))
            .should("have.text", firstName);
        cy.get(this.getTdPath(row, 2))
            .should("have.text", lastName);
        cy.get(this.dialogBox_locator)
            .should("not.exist");
    }

    editEmailCorrectInput(row, email){
        this.openEditWindow(row);
        cy.get(this.email_locator).clear();
        this.typeEmail(email);
        cy.get(this.submitButton_locator)
            .click();
        expect(email).to.match(/([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})/);
        cy.get(this.dialogBox_locator)
            .should("not.exist");
        cy.get(this.getTdPath(row, 4))
            .should("have.text", email);
    }

    editEmailWrongInput(row, email){
        this.openEditWindow(row);
        cy.get(this.email_locator)
            .clear();
        this.typeEmail(email);
        cy.get(this.submitButton_locator)
            .click();
        expect(email).not.to.match(/([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})/);
        cy.get(this.dialogBox_locator)
            .should("exist");
        cy.get(this.getTdPath(row, 4))
            .should("not.have.text", email);
        this.closeEditWindow();
        cy.get(this.dialogBox_locator)
            .should("not.exist");
    }

    typeEmail(email){
        cy.get(this.email_locator)
            .type(email);
    }

    typeAge(age){
        cy.get(this.age_locator)
            .type(age);
    }
    
    typeSalary(salary){
        cy.get(this.salary_locator)
            .type(salary);
    }

    typeDepartment(department){
        cy.get(this.department_locator)
            .type(department);
    }
    
    addNewRecord(user){
        cy.get(this.addButton_locator)
            .click({force: true});
        this.typeFirstName(user.firstName);
        this.typeLastName(user.lastName);
        this.typeEmail(user.email);
        this.typeAge(user.age);
        this.typeSalary(user.salary);
        this.typeDepartment(user.department);
        cy.get(this.submitButton_locator)
            .click();
    }

    showMoreRows(numberOfRows){
        cy.get("select")
            .select(numberOfRows + " rows");
        cy.get(this.row_locator).its("length")
            .should("equal", numberOfRows);
    }

    search(word){
        cy.get(this.searchbox_locator)
            .type(word + "{downarrow}{enter}");
        cy.get(this.row_locator)
            .each(($e, $index, $list) => {
                var text = $e.text();
                cy.get(this.row_locator).contains(word);
            });
    }

    pressSearchButton(word){
        cy.get(this.searchbox_locator)
            .type(word);
        this.searchButton_locator
            .click();
        cy.get(this.row_locator)
            .each(($e, $index, $list) => {
                var text = $e.text();
                cy.get(this.row_locator).contains(word);
            });
    }
}