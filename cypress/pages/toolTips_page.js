export class ToolTips_page{
    // locators
    element_locator;
    message_locator;
    // methods
    navigate(url){
        cy.visit(url);
    }

    hover(element){
        this.setLocators(element);
        cy.get(this.element_locator)
            .trigger("mouseover");
    }

    messageVisible(){
        cy.get(this.message_locator)
        .should("be.visible");
    }

    setLocators(key){
        switch (key) {
            case "button":
                this.element_locator = "#toolTipButton";
                this.message_locator  = "#buttonToolTip";
                break;
            case "textField":
                this.element_locator = "#toolTipTextField";
                this.message_locator = "#textFieldToolTip";
                break;
            case "contrary":
                this.element_locator = "#texToolTopContainer > :nth-child(1)";
                this.message_locator = "#contraryTexToolTip";
                break;
            case "sections":
                this.element_locator = "#texToolTopContainer > :nth-child(2)";
                this.message_locator = "#sectionToolTip";
                break;
            default:
                this.element_locator = "";
                this.message_locator = "";
                break;
        }
    }
}