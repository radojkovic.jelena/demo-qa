export class Tabs_Page{
    // locators
    body_locator = "#demo-tabpane-";
    element_locator = "#demo-tab-";
    tabMore_locator = "#demo-tab-more";    
    // methods
    navigate(url){
        cy.visit(url);
    }

    display(tab){
        cy.get(this.element_locator + tab)
            .click();
    }

    bodyVisible(tab){
        cy.get(this.body_locator + tab)
            .should("be.visible");
    }

    bodyNotVisible(tab){
        cy.get(this.body_locator + tab)
            .should("not.be.visible");
    }
    // more tab
    tabMoreDisabled(){
        cy.get(this.tabMore_locator)
            .invoke("attr", "aria-disabled")
            .should("equal", "true");
    }
}