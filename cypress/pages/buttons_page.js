export class Buttons_Page{
    // locators
    doubleClick_locator = "#doubleClickBtn";
    rightClick_locator = "#rightClickBtn";
    dynamicDiv_locator = ".col-md-6 > :nth-child(1) > :nth-child(3)";
    doubleClickMsg_locator = "#doubleClickMessage";
    rightClickMsg_locator = "#rightClickMessage";
    dynamicMsg = "#dynamicClickMessage";
    // methods
    navigate(url){
        cy.visit(url);
    }

    doubleClick(){
        cy.get(this.doubleClick_locator)
            .dblclick();
        cy.get(this.doubleClickMsg_locator)
            .should("exist")
            .and("contain", "double click");
    }

    rightClick(){
        cy.get(this.rightClick_locator)
            .rightclick();
        cy.get(this.rightClickMsg_locator)
            .should("exist")
            .and("contain", "right click");
    }

    dynamicClick(){
        cy.get(this.dynamicDiv_locator)
            .find(">button")
            .click();
        cy.get(this.dynamicMsg)
            .should("exist")
            .and("contain", "dynamic click");
    }
}