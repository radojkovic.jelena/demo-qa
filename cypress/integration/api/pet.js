/// <reference types = "cypress" />
Cypress.config("baseUrl", "https://petstore.swagger.io/v2");

const faker = require("faker");
const id = faker.datatype.number({min: 0, max: 10})
const petName = faker.name.firstName();
const newPetName = faker.name.firstName();
const categoryId = faker.datatype.number({min: 0, max: 5}) 
const image = faker.image.animals();
const status = "available";
const categories = ["Beagel", "Boxer", "Bulldog", "Dalmatian", "Maltese", "Pug"];

function getPet($id, $petName, $categoryId, $image, $status){
    let item = {
        "id": $id,
        "category": {
            "id": $categoryId,
            "name": categories[$categoryId]
        },
        "name": $petName,
        "photoUrls": [
            $image
        ],
        "tags": [
            {
            "id": $categoryId,
             "name": "Tag: " + $categoryId + " " + categories[$categoryId]
            }
        ],
        "status": $status
    }
    return item;
}

describe("Pet Store Test Scenario", function () {
    it("Create a new Pet", function () {
        let pet = getPet(id, petName, categoryId, image, status);
        cy.request({
            method: "POST",
            url: "/pet",
            body: pet
        })
        .its("body")
        .should("deep.equal", pet);
    });

    it("Update Pet name", function () {
        let pet = getPet(id, newPetName, categoryId, image, status);
        cy.request({
            method: "PUT",
            url: "/pet",
            body: pet
        })
        .its("body")
        .should("deep.equal", pet);
    })

    it("Delete the Pet", function () {
        cy.request({
            method: "DELETE",
            url: "/pet/" + id
        })
        .its("body")
        .its("message")
        .should("equal", id.toString());

        cy.request({
            method: "DELETE",
            url: "/pet/" + id
        })
        .its("body")
        .its("message")
        .should("equal", id.toString());

        cy.request({
            method: "GET",
            url: "/pet/" + id,
            failOnStatusCode: false
        })
        .its("status")
        .should("equal", 404);
    });
});