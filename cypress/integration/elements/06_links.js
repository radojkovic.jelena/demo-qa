/// <reference types = "cypress" />
import { Links_Page } from "../../pages/links_page";
const links_page = new Links_Page();

describe("Testing Links page", function () {
  before(function () {
    links_page.navigate("/links");
  });

  it("Open new tabs", function () {
    links_page.clickHome();
    links_page.clickHomerAF6n();
  });

  it("Send API calls - Positive", function () {
    links_page.clickCreated();
    links_page.clickNoContent();
    links_page.clickMoved();
  });
  /*
    it("Send API calls - Bad Request", function () {
        links_page.navigate(url);
        links_page.clickBadRequest();
    })

    it("Send API calls - Unauthorized", function () {
        links_page.navigate(url);
        links_page.clickUnauthorized();
    })
    */
});
