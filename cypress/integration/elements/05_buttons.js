/// <reference types = "cypress" />
import { Buttons_Page } from "../../pages/buttons_page";

const buttons_page = new Buttons_Page();

describe("Testing Buttons page", function () {
  before(function () {
    buttons_page.navigate("/buttons");
  });

  it("Double click", function () {
    buttons_page.doubleClick();
  });

  it("Right click", function () {
    buttons_page.rightClick();
  });

  it("Dynamic click", function () {
    buttons_page.dynamicClick();
  });
});
