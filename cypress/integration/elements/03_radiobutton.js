/// <reference types = "cypress" />
import { RadioButton_Page } from "../../pages/radiobutton_page";

const radiobutton_page = new RadioButton_Page();

describe("Testing Radio Button page", function () {
  before(function () {
    radiobutton_page.navigate("/radio-button");
  });

  it("Select Yes button", function () {
    radiobutton_page.clickYes();
  });

  it("Select Impressive button", function () {
    radiobutton_page.clickImpressive();
  });

  it("Hover No button", function () {
    radiobutton_page.hoverNo();
  });
});
