/// <reference types = "cypress" />
import { CheckBox_Page } from "../../pages/checkbox_page";

const checkbox_page = new CheckBox_Page();

describe("Testing Check Box page", function () {
  before(function () {
    checkbox_page.navigate("/checkbox");
  });

  it("Expand/Collapse one by one", function () {
    // expand
    checkbox_page.homeListExpand();
    checkbox_page.desktopListExpand();
    checkbox_page.documentsListExpand();
    checkbox_page.downloadsListExpand();
    // collapse
    checkbox_page.downloadsListCollapse();
    checkbox_page.documentsListCollapse();
    checkbox_page.desktopListCollapse();
    checkbox_page.homeListCollapse();
  });

  it("Expand/collapse all", function () {
    checkbox_page.expandAll();
    checkbox_page.collapseAll();
  });

  it("Check/uncheck notes", function () {
    checkbox_page.expandAll();
    checkbox_page.checkNotes();
    checkbox_page.uncheckNotes();
  });

  it("Check/uncheck commands", function () {
    checkbox_page.expandAll();
    checkbox_page.checkCommands();
    checkbox_page.uncheckCommands();
  });

  it("Check both notes & commands", function () {
    checkbox_page.expandAll();
    checkbox_page.checkNotesAndCommands();
  });

  it("Check desktop", function () {
    checkbox_page.expandAll();
    checkbox_page.checkDesktop();
  });
});
