/// <reference types = "cypress" />
import { WebTables_Page } from "../../pages/webtables_page";

const webtables_page = new WebTables_Page();
let user;
let wrongEmail = "example@123";
let searchWord = "ier";

describe("Testing Web Tables page", function () {
  before(function () {
    cy.visit("/webtables");
    cy.task("getUser").then((object) => {
      user = object;
    });
  });

  it("Delete row", function () {
    webtables_page.deleteRow(2);
  });

  it("Open/close edit window", function () {
    webtables_page.openEditWindow(3);
    webtables_page.closeEditWindow();
  });

  it("Edit first and last name", function () {
    webtables_page.editFirstAndLastName(1, user.firstName, user.lastName);
  });

  it("Add new record", function () {
    webtables_page.addNewRecord(user);
  });

  it("Edit email - positive test", function () {
    webtables_page.editEmailCorrectInput(1, user.email);
  });

  it("Edit email - negative test", function () {
    webtables_page.navigate("/webtables");
    webtables_page.editEmailWrongInput(1, wrongEmail);
  });

  it("Show more rows", function () {
    webtables_page.showMoreRows(50);
  });

  it("Search", function () {
    webtables_page.search(searchWord);
  });
});
