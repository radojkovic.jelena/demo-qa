/// <reference types = "cypress" />
import { TextBox_Page } from "../../pages/textbox_page";

const textbox_page = new TextBox_Page();
let user;
let wrongEmail = "example@123";

describe("Testing Text Box page", function () {
  before(function () {
    cy.task("getUser").then((object) => {
      user = object;
    });
  });

  it("Correct email - Positive test", function () {
    textbox_page.navigate("/text-box");
    textbox_page.enterFullName(user.firstName + " " + user.lastName);
    textbox_page.enterEmail(user.email);
    textbox_page.emailShouldMatchRegEx();
    textbox_page.enterCurrentAddress(user.address);
    textbox_page.enterPermanentAddress(user.address);
    textbox_page.clickSubmit();
    textbox_page.emailBorderNeutral();
    textbox_page.outputVisible();
    textbox_page.validateOutputName();
    textbox_page.validateOutputEmail();
    textbox_page.validateOutputCurrentAddress();
    textbox_page.validateOutputPermanentAddress();
  });

  it("Wrong email - Negative test", function () {
    textbox_page.navigate("/text-box");
    textbox_page.enterFullName(user.firstName + " " + user.lastName);
    textbox_page.enterEmail(wrongEmail);
    textbox_page.emailShouldNotMatchRegEx();
    textbox_page.enterCurrentAddress(user.address);
    textbox_page.enterPermanentAddress(user.address);
    textbox_page.clickSubmit();
    textbox_page.emailBorderRed();
    textbox_page.outputInvisible();
  });
});
