/// <reference types = "cypress" />
const { Date_Page } = require("../../pages/date_page");

const date_page = new Date_Page();
const faker = require("faker");

let year = faker.datatype.number({ 'min': 1900, 'max': 2100 });
let day = faker.datatype.number({ 'min': 1, 'max': 28 });
let yearOption = faker.datatype.number({ 'min': 2, 'max': 12 });
let monthOption = faker.datatype.number({ 'min': 1, 'max': 12 });
let hour = faker.datatype.number({ 'min': 0, 'max': 24 });
let step = faker.datatype.number({ 'min': 0, 'max': 3 });

describe("Testing Date picker", function () {
    before(function () {
        date_page.navigate("/date-picker");
        date_page.setLocators("date");
    });

    it("Open calendar", function () {
        date_page.clickDatePicker();
        date_page.calendarShouldExist();
    });

    it("Choose year", function () {
        date_page.clickDatePicker();
        date_page.selectYear(year);
        date_page.validateYearLabel(year);
    });

    it("Choose month", function () {
        date_page.selectMonth(monthOption);
        date_page.validateMonthLabel(monthOption);
    });
    
    it("Choose day", function () {
        date_page.chooseDay(day);
        date_page.calendarShouldNotExist();
        date_page.validateDisplayedDate();
    });

    it("Click arrow next", function () {
        date_page.clickDatePicker();
        date_page.selectMonth(monthOption);
        date_page.validateMonthLabel(monthOption);
        date_page.clickArrowNext();
        let nextMonthIndex = date_page.getNextMonthIndex(monthOption);
        date_page.validateSelectedMonthIndex(nextMonthIndex);
    });

    it("Click arrow previous", function () {
        date_page.clickDatePicker();
        date_page.selectMonth(monthOption);
        date_page.validateMonthLabel(monthOption);
        date_page.clickArrowPrevious();
        let prevMonthIndex = date_page.getPreviousMonthIndex(monthOption);
        date_page.validateSelectedMonthIndex(prevMonthIndex);
    });
});

describe("Testing Date and Time picker", function () {
    before(function () {
        date_page.navigate("/date-picker");
        date_page.setLocators("time");
    });

    it("Open calendar", function () {
        date_page.clickDatePicker();
        date_page.calendarShouldExist();
    });

    it("Choose year", function () {
        date_page.expandYearDropDown();
        date_page.yearDropDownExpanded();
        date_page.selectYearDropDown(yearOption);
        date_page.yearSelected(yearOption);
        date_page.validateYearLabel(yearOption);
    });
        
    it("Choose month", function () {
        date_page.expandMonthDropDown();
        date_page.monthDropDownExpanded();
        date_page.selectMonthDropDown(monthOption);
        date_page.monthSelected(monthOption);
        date_page.validateMonthLabel(monthOption);
    });
    
    it("Choose day and time", function () {
        let minute = step * 15;
        date_page.chooseDay(day);
        date_page.chooseTime(hour, minute);
        date_page.calendarShouldNotExist();
        date_page.validateDisplayedDateAndTime();
    });

    it("Click arrow next", function () {
        date_page.clickDatePicker();
        date_page.expandMonthDropDown();
        date_page.monthDropDownExpanded();
        date_page.selectMonthDropDown(monthOption);
        date_page.clickArrowNext();
        let nextMonthIndex = date_page.getNextMonthIndex(monthOption);
        date_page.validateSelectedMonthIndex(nextMonthIndex, 1);
    });

    it("Click arrow previous", function () {
        date_page.clickDatePicker();
        date_page.expandMonthDropDown();
        date_page.monthDropDownExpanded();
        date_page.selectMonthDropDown(monthOption);
        date_page.clickArrowPrevious();
        let prevMonthIndex = date_page.getPreviousMonthIndex(monthOption);
        date_page.validateSelectedMonthIndex(prevMonthIndex , 1);
    });
});