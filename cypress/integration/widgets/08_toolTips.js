/// <reference types = "cypress" />
import { ToolTips_page } from "../../pages/toolTips_page";

const tooltips_page = new ToolTips_page(ToolTips_page);

describe("Testing Tool Tips page", function () {
    before(function () {
        tooltips_page.navigate("/tool-tips");
    });

    it("Hover button", function () {
        tooltips_page.hover("button");
        tooltips_page.messageVisible();
    });

    it("Hover text field", function () {
        tooltips_page.hover("textField");
        tooltips_page.messageVisible();
    });

    it("Hover contrary text", function () {
        tooltips_page.hover("contrary");
        tooltips_page.messageVisible();
    });

    it("Hover sections", function () {
        tooltips_page.hover("sections");
        tooltips_page.messageVisible();
    });
});