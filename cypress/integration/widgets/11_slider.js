/// <reference types = "cypress" />
import { Slider_Page } from "../../pages/slider_page";

const slider_page = new Slider_Page();
const faker = require("faker");
let val = faker.datatype.number({min: 0, max: 100});
let lowerVal = faker.datatype.number({min: -100, max: -1});
let higherVal = faker.datatype.number({min: 101, max: 200});

describe("Testing Slider", function () {
    before(function () {
        slider_page.navigate("/slider");
    });

    it("Value in expected range", function () {
        slider_page.setNewValue(val);
        slider_page.newValueVisible(val);
    });

    it("Value equal to lower limit", function () {
        slider_page.setNewValue(0);
        slider_page.newValueVisible(0);
    });

    it("Value equal to upper limit", function () {
        slider_page.setNewValue(100);
        slider_page.newValueVisible(100);
    });

    it("Value below the lower limit", function () {
        slider_page.setNewValue(lowerVal);
        slider_page.newValueVisible(lowerVal);
    });

    it("Value above the upper limit", function () {
        slider_page.setNewValue(higherVal);
        slider_page.newValueVisible(higherVal);
    });
});