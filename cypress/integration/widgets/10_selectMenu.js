/// <reference types = "cypress" />
import { SelectMenu_Page } from "../../pages/selectMenu_page";

const selectMenu_page = new SelectMenu_Page();
const faker = require("faker");

let group;
let option;

function getNumber(low, high){
    return faker.datatype.number({min: low, max: high});
}

describe("Testing Select Value Drop Down", function () {
    before(function () {
        selectMenu_page.navigate("/select-menu");
        selectMenu_page.setLocators("selectValue");
        group = getNumber(0, 3);
        option = getNumber(0, 1);
    });

    it("Select option", function () {
        selectMenu_page.expandDropDown();
        selectMenu_page.expandedBodyExists();
        selectMenu_page.selectOption(group, 2, option);
        selectMenu_page.optionSelected(group, 2, option);
    })

    it("Type option", function () {
        selectMenu_page.typeOption(group, 2, option);
        selectMenu_page.optionSelected(group, 2, option);
    });
});

describe("Testing Select One Drop Down", function () {

    before(function () {
        selectMenu_page.navigate("/select-menu");
        selectMenu_page.setLocators("selectOne");
        option = getNumber(0, 5);
    });

    it("Select one option", function (){
        selectMenu_page.expandDropDown();
        selectMenu_page.expandedBodyExists();
        selectMenu_page.selectOption(option, 3);
        selectMenu_page.optionSelected(option, 3);
    });

    it("Type one option", function (){
        selectMenu_page.typeOption(option, 3);
        selectMenu_page.optionSelected(option, 3);
    });
});

describe("Testing Old Style Select Menu", function () {

    before(function () {
        selectMenu_page.navigate("/select-menu");
        option = getNumber(1, 10)
    });

    it("Select old style menu", function () {
        selectMenu_page.oldStyleSelectOption(option);
        selectMenu_page.oldStyleOptionSelected(option);
    });
});
    
describe("Testing Multiselect drop down", function() {

    before(function () {
        selectMenu_page.navigate("/select-menu");
        selectMenu_page.setLocators("multiSelectDropDown");
        option = getNumber(0, 3);
    });

    it("Select from drop down", function () {
        selectMenu_page.expandDropDown();
        selectMenu_page.expandedBodyExists();
        selectMenu_page.selectOption(option, 4);
        selectMenu_page.optionSelected(option, 4);
        selectMenu_page.selectedOptiNotVisible(option, 4);
    });

    it("Type", function () {
        selectMenu_page.navigate("/select-menu");
        selectMenu_page.typeOption(option, 4);
        selectMenu_page.selectedOptiNotVisible(option, 4);
    });

    it("Remove all", function () {
        selectMenu_page.removeAll();
        selectMenu_page.allOptionsVisible();
    });
});

describe("Testing Standard multi select", function () {
    before(function () {
        selectMenu_page.navigate("/select-menu");
        option = getNumber(0, 3);
    });

    it("Select two random options", function () {
        let option1 = getNumber(0, 3);
        selectMenu_page.selectTwoOptions(option, option1);
        selectMenu_page.twoOptionsSelected(option, option1);
    });

    it("Select all options", function () {
        selectMenu_page.selectAllOptions();
        selectMenu_page.allOptionsSelected();
    });
});