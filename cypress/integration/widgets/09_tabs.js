/// <reference types = "cypress" />
import { Tabs_Page } from "../../pages/tabs_page";

const tabs_page = new Tabs_Page();

describe("Testing Tabs", function () {
    before(function () {
        tabs_page.navigate("/tabs");
    });

    it("Click What Tab", function () {
        tabs_page.display("what");
        tabs_page.bodyVisible("what");
        tabs_page.bodyNotVisible("origin");
        tabs_page.bodyNotVisible("use");
        tabs_page.tabMoreDisabled();
    });

    it("Click Origin Tab", function () {
        tabs_page.display("origin");
        tabs_page.bodyVisible("origin");
        tabs_page.bodyNotVisible("what");
        tabs_page.bodyNotVisible("use");
        tabs_page.tabMoreDisabled();
    });

    it("Click Use Tab", function () {
        tabs_page.display("use");
        tabs_page.bodyVisible("use");
        tabs_page.bodyNotVisible("what");
        tabs_page.bodyNotVisible("origin");
        tabs_page.tabMoreDisabled();
    });
});